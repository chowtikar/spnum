//
//  BaseViewController.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 27/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import Firebase

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if FirebaseApp.app() == nil {
           FirebaseApp.configure()
       }
    }
}
