//
//  Color.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let progressbarTint = UIColor.hex("#289ECE")
    static let progressbarBg = UIColor.hex("#C7C7C9")
    static let textDarkGray = UIColor.hex("#4F5051")
    static let textGreen = UIColor.hex("#2C845E")
    static let textRed = UIColor.hex("#EB4039")
    static let textYellow = UIColor.hex("#E1C335")
    
    static let viewBackground = UIColor.hex("#F4F5F6")
    
    static func hex(_ hex: String) -> UIColor {
          var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
          
          if cString.hasPrefix("#") {
              cString.remove(at: cString.startIndex)
          }
          
          if cString.count != 6 {
              return UIColor.gray
          }
          
          var rgbValue: UInt32 = 0
          Scanner(string: cString).scanHexInt32(&rgbValue)
          
          return UIColor(
              red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
              green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
              blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
              alpha: CGFloat(1.0)
          )
      }
      
}
