//
//  CaseListViewController.swift
//
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class CaseListViewController: BaseViewController {
    
    @IBOutlet weak var caseListTableView: UITableView!
    @IBOutlet weak var createListButton: UIButton!
    @IBOutlet weak var userButton: UIButton!
    
    var manager: CaseListManager?
    var caseList: [CaseListModel.CaseModel]?
    var userList: [LoginModel.AccountModel]?
    var isShowUser: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = CaseListManager()
        setupView()
        caseClickActionHandler(UIButton())
    }
    
    private func setupView() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutTapped))
        self.view.backgroundColor = UIColor.hex("#F2F3F7")
        caseListTableView.register(UINib(nibName: "CaseTableViewCell", bundle: nil), forCellReuseIdentifier: "CaseTableViewCell")
        caseListTableView.separatorStyle = .none
        caseListTableView.delegate = self
        caseListTableView.dataSource = self
        caseListTableView.backgroundColor = .clear
        
        createListButton.backgroundColor = .white
        createListButton.layer.cornerRadius = 22
        createListButton.layer.borderColor = UIColor.white.cgColor
        createListButton.layer.borderWidth = 1
        createListButton.setTitle("CASES", for: .normal)
        createListButton.setTitleColor(.textDarkGray, for: .normal)
        createListButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        createListButton.layer.applyShadow(color: .black, alpha: 0.25)
        
        userButton.backgroundColor = .white
        userButton.layer.cornerRadius = 22
        userButton.layer.borderColor = UIColor.white.cgColor
        userButton.layer.borderWidth = 1
        userButton.setTitle("USERS", for: .normal)
        userButton.setTitleColor(.textDarkGray, for: .normal)
        userButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        userButton.layer.applyShadow(color: .black, alpha: 0.25)
    }
    
    private func routeToRegister() {
        let view = RegisterViewController()
        view.modalPresentationStyle = .fullScreen
        self.present(view, animated: true, completion: nil)
    }
    
    @objc func logoutTapped() {
        let alert = UIAlertController(title: "", message: "Do you want to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            LoginModel.profileInfo = nil
            let sView = SplashscreenViewController()
            Utils.changeRootViewController(sView)
        }))
        self.present(alert, animated: true)
    }
    
    @IBAction func caseClickActionHandler(_ sender: UIButton) {
        title = "Case List"
        isShowUser = false
        createListButton.isUserInteractionEnabled = false
        userButton.isUserInteractionEnabled = false
        manager?.getCases(successHandle: { (status, cases) in
            if status {
                self.caseList = cases
                self.caseListTableView.reloadData()
            }
            self.createListButton.isUserInteractionEnabled = true
            self.userButton.isUserInteractionEnabled = true
        }, errorHandle: { message in
            let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        })
    }
    
    @IBAction func userClickActionHandler(_ sender: Any) {
        title = "Patient List"
        isShowUser = true
        createListButton.isUserInteractionEnabled = false
        userButton.isUserInteractionEnabled = false
        manager?.getUser(successHandle: { users in
            self.userList = users
            self.caseListTableView.reloadData()
            self.createListButton.isUserInteractionEnabled = true
            self.userButton.isUserInteractionEnabled = true
        },  errorHandle: { message in
            let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        })
    }
}

extension CaseListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isShowUser ? self.userList?.count ?? 0 : self.caseList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isShowUser {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CaseTableViewCell", for: indexPath) as? CaseTableViewCell, let data = userList?[indexPath.row] else {
                fatalError()
            }
            cell.selectionStyle = .none
            cell.setupData(userData: data)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CaseTableViewCell", for: indexPath) as? CaseTableViewCell, let data = caseList?[indexPath.row] else {
                fatalError()
            }
            cell.selectionStyle = .none
            cell.setupData(caseInfo: data)
            return cell
        }
    }
}

extension CaseListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isShowUser {
            guard let data = userList?[indexPath.row] else { return }
            routeToUser(info: data)
        } else {
            guard let data = caseList?[indexPath.row] else { return }
            routeToCase(info: data)
        }
    }
    
    func routeToCase(info: CaseListModel.CaseModel) {
        let view = CaseInfoViewController()
        view.updateInfomation(caseInfo: info)
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func routeToUser(info: LoginModel.AccountModel) {
       let view = UserDetailsViewController()
        view.setupUser(info: info)
        self.navigationController?.pushViewController(view, animated: true)
    }
}
