//
//  CaseListModel.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import Foundation

struct CaseListModel {
    struct CaseModel: Codable {
        var id, createTime, imageUrl, message, patientId, name, lastname, gender : String?
        var age: Int
        var isDone: Bool = false
        
        init() {
            let proInfo = LoginModel.profileInfo
            id = ""
            createTime = ""
            imageUrl = ""
            message = ""
            patientId = proInfo?.ID ?? ""
            name = proInfo?.Name ?? ""
            lastname = proInfo?.Surname ?? ""
            gender = proInfo?.Gender ?? "F"
            age = proInfo?.Age ?? 0
            isDone = false
        }
        
        func getDict() -> [String: Any] {
           
            let dict = ["id": id ?? 0,
                        "createTime": NSDate().description,
                        "imageUrl": imageUrl ?? "",
                        "isDone": isDone,
                        "message": message ?? "",
                        "patientId": patientId ?? "",
                        "age": age ?? 0,
                        "gender": gender ?? "M",
                        "name": name ?? "",
                        "lastname": lastname ?? ""
                ] as [String : Any]
            
            return dict
        }
        
        func checkIsCorrect() -> Bool {
            let notNull = id != nil && createTime != nil && imageUrl != nil && message != nil
            let notEmpty = id != "" && message != ""
            return notNull && notEmpty
        }
    }
}
