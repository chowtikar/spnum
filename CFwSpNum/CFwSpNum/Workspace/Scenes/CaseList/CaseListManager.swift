//
//  CaseListManager.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import Firebase
import CodableFirebase

class CaseListManager {
    
    let database = Database.database()
    
    func getCases(successHandle: @escaping ((Bool, [CaseListModel.CaseModel]) -> Void), errorHandle: ((String) -> Void)? = nil) {
        database.reference(withPath: "Case").observe(.value) { data in
            guard let value = data.value else { return }
            do {
                let accList = try FirebaseDecoder().decode([String: CaseListModel.CaseModel].self, from: value)
                var cases: [CaseListModel.CaseModel] = []
                for i in accList {
                    cases.append(i.value)
                }
                successHandle(true, cases)
            } catch let error {
                errorHandle?(error.localizedDescription)
            }
        }
    }
    
    
    func getUser(successHandle: @escaping (([LoginModel.AccountModel]) -> Void), errorHandle: ((String) -> Void)? = nil) {
        database.reference(withPath: "Account").observe(.value) { data in
            guard let value = data.value else { return }
            do {
                let accList = try FirebaseDecoder().decode([String: LoginModel.AccountModel].self, from: value)
                var accounts: [LoginModel.AccountModel] = []
                for i in accList {
                    if i.value.Status == "P" {
                        accounts.append(i.value)
                    }
                }
                successHandle(accounts)
            } catch let error {
                errorHandle?(error.localizedDescription)
            }
        }
    }
}

