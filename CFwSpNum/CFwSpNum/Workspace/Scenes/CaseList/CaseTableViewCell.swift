//
//  CaseTableViewCell.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 25/10/2563 BE.
//

import UIKit
import Kingfisher
import SwiftDate

class CaseTableViewCell: UITableViewCell {
    
    @IBOutlet var contentViewCell: UIView!
    @IBOutlet weak var caseImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        contentViewCell.layer.cornerRadius = 16
        contentViewCell.clipsToBounds = true
        caseImage.image = UIImage(named: "image-placeholder")
        caseImage.contentMode = .scaleAspectFit
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        titleLabel.textColor = .darkText
        titleLabel.text = ""
        subTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        subTitleLabel.textColor = .gray
        subTitleLabel.text = ""
    }
    
    func setupData(caseInfo: CaseListModel.CaseModel) {
        titleLabel.text = caseInfo.message ?? ""
        subTitleLabel.text = getDate(str: caseInfo.createTime ?? "")
        caseImage.isHidden = false
        guard let imgUrl = caseInfo.imageUrl else { return }
        let url = URL(string: imgUrl)!
        downloadImage(from: url)
    }
    
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.caseImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func getDate(str: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm dd MMM yyyy"
        
        if let date = dateFormatterGet.date(from: str) {
            return ("\(dateFormatterPrint.string(from: date + 7.hours))")
        } else {
            return ""
        }
    }
    
    func setupData(userData: LoginModel.AccountModel) {
        caseImage.isHidden = true
        titleLabel.text = "\(userData.Name ?? "") \(userData.Surname ?? "")"
        subTitleLabel.text = "Age: \(userData.Age ?? 0) Gender: \(userData.Gender == "M" ? "Male" : "Female")"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            self.init(data: try Data(contentsOf: url))
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}
