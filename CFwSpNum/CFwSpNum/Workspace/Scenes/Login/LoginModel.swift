//
//  LoginModel.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import Foundation

struct LoginModel {
    
    static var profileInfo: AccountModel? {
        get {
            if let savedProfile = UserDefaults.standard.object(forKey: "savedProfileData") as? Data {
                let decoder = JSONDecoder()
                return try?  decoder.decode(AccountModel.self, from: savedProfile)
            }
            return nil
        } set {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "savedProfileData")
            }
        }
    }
    
    struct AccountModel: Codable {
        var Age: Int?
        var Date, Email, Gender, ID, Name, PW, Phone, Status, Surname, Usr: String?
        
        func checkIsCorrect() -> Bool {
            let notNull = Age != nil && Email != nil && Gender != nil && Name != nil && PW != nil && Phone != nil && Status != nil && Surname != nil && Usr != nil
            let notEmpty = (Age ?? 0) > 0 && Email != "" && Gender != "" && Name != "" && PW != "" && Phone != "" && Status != "" && Surname != "" && Usr != ""
            return notNull && notEmpty
        }
    }
    
    struct Error: Decodable {
        
    }
    
}
