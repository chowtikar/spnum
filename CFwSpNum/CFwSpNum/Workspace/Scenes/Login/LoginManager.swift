//
//  LoginManager.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import FirebaseDatabase
import CodableFirebase

class LoginManager {
    
    let database = Database.database().reference(withPath: "Account")
    var username: String = ""
    var password: String = ""
    
    func currentLogin(usr: String, pass: String) {
        username = usr
        password = pass
    }
    
    func authentication(email: String, password: String, successHandle: @escaping ((Bool, String) -> Void), errorHandle: ((String) -> Void)? = nil) {
        database.observe(.value) { data in
            guard let value = data.value else { return }
            do {
                let accList = try FirebaseDecoder().decode([String: LoginModel.AccountModel].self, from: value)
                var account: LoginModel.AccountModel?
                for i in accList {
                    if i.value.Usr == email && i.value.PW == password {
                        account = i.value
                    }
                }
                LoginModel.profileInfo = account
                successHandle(account != nil, account?.Status ?? "P")
            } catch let error {
                errorHandle?(error.localizedDescription)
            }
        }
    }
}
