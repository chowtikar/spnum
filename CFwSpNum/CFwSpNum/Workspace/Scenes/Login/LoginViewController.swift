//
//  LoginViewController.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    var manager: LoginManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = LoginManager()
        setupView()
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor.hex("#F2F3F7")
        
        indicatorView.style = .large
        indicatorView.color = .gray
        indicatorView.isHidden = true
        indicatorView.startAnimating()
        
        usernameTextField.placeholder = "Username"
        usernameTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        usernameTextField.lineColor = .darkGray
        usernameTextField.textColor = .darkText
        usernameTextField.placeholderColor = .gray
        usernameTextField.textAlignment = .center
        
        passwordTextField.placeholder = "Password"
        passwordTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        passwordTextField.lineColor = .darkGray
        passwordTextField.textColor = .darkText
        passwordTextField.placeholderColor = .gray
        passwordTextField.textAlignment = .center
        passwordTextField.isSecureTextEntry = true
        
        loginButton.backgroundColor = .white
        loginButton.layer.cornerRadius = 22
        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.borderWidth = 1
        loginButton.setTitle("Login", for: .normal)
        loginButton.setTitleColor(.textDarkGray, for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        loginButton.layer.applyShadow(color: .black, alpha: 0.25)
        
        registerButton.backgroundColor = .white
        registerButton.layer.cornerRadius = 22
        registerButton.layer.borderColor = UIColor.lightGray.cgColor
        registerButton.layer.borderWidth = 2
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(.lightGray, for: .normal)
        registerButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        registerButton.layer.applyShadow(color: .gray, alpha: 0.25)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    private func isLoading(_ loading: Bool) {
        indicatorView.isHidden = !loading
        usernameTextField.isUserInteractionEnabled = !loading
        passwordTextField.isUserInteractionEnabled = !loading
        loginButton.isUserInteractionEnabled = !loading
    }
    
    @IBAction func logInActionHandler(_ sender: Any) {
        guard let user = usernameTextField.text, let pss = passwordTextField.text else { return }
        isLoading(true)
        manager?.authentication(email: user, password: pss, successHandle: { [weak self] result, status  in
            guard let self = self else { return }
            self.isLoading(false)
            if result {
                if status == "P" {
                    self.routeToCreateCase()
                } else {
                    self.routeToCases()
                }
            }
        })
    }
    
    @IBAction func registerActionHandler(_ sender: Any) {
        routeToRegister()
    }
    
    private func routeToRegister() {
        let view = RegisterViewController()
        view.modalPresentationStyle = .fullScreen
        self.present(view, animated: true, completion: nil)
    }
    
    private func routeToCases() {
        let view = CaseListViewController()
        view.modalPresentationStyle = .fullScreen
        let nav = UINavigationController(rootViewController: view)
        Utils.changeRootViewController(nav)
    }
    
    private func routeToCreateCase() {
        let view = CaseDetailsViewController()
        view.modalPresentationStyle = .fullScreen
        let nav = UINavigationController(rootViewController: view)
        Utils.changeRootViewController(nav)
    }
}
