//
//  SplashscreenViewController.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class SplashscreenViewController: BaseViewController {
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    var manager: SplashscreenManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicatorView.startAnimating()
        guard (manager == nil) else { return }
        manager = SplashscreenManager()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.indicatorView.stopAnimating()
        guard let account = LoginModel.profileInfo else {
            routeToLoginVC()
            return
        }
        if account.Status == "P" {
            routeToCreateCase()
        } else {
            routeToCases()
        }
    }
    
    private func setupView() {
        indicatorView.style = .large
        indicatorView.color = .gray
        indicatorView.isHidden = false
        indicatorView.startAnimating()
    }
    
    private func routeToLoginVC() {
        indicatorView.isHidden = true
        let view = LoginViewController()
        view.modalPresentationStyle = .fullScreen
        self.present(view, animated: true, completion: nil)
    }
    
    private func routeToCases() {
        let view = CaseListViewController()
        view.modalPresentationStyle = .fullScreen
        let nav = UINavigationController(rootViewController: view)
        Utils.changeRootViewController(nav)
    }
    
    private func routeToCreateCase() {
        let view = CaseDetailsViewController()
        view.modalPresentationStyle = .fullScreen
        let nav = UINavigationController(rootViewController: view)
        Utils.changeRootViewController(nav)
    }
}
