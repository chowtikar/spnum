//
//  CaseDetailsViewController.swift
//
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class CaseDetailsViewController: BaseViewController {
    
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var caseDetailsImage: UIImageView!
    @IBOutlet weak var caseDetailsTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    
    var vc: UIImagePickerController?
    var selectImage: UIImage?
    var manager: CaseDetailsManager?
    var isDoneUpload = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = CaseDetailsManager()
        setupView()
    }
    
    private func setupView() {
        title = "Case Information"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutTapped))
        self.view.backgroundColor = .white
        caseDetailsImage.image = UIImage(named: "image-placeholder")
        caseDetailsTextView.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        caseDetailsTextView.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        caseDetailsTextView.delegate = self
        caseDetailsTextView.layer.cornerRadius = 16
        caseDetailsTextView.clipsToBounds = true
        caseDetailsTextView.text = "Please put you case details"
        caseDetailsTextView.textColor = .gray
        caseDetailsTextView.backgroundColor = .viewBackground
        
        doneButton.layer.cornerRadius = 22
        doneButton.layer.borderColor = UIColor.lightGray.cgColor
        doneButton.layer.borderWidth = 1
        doneButton.setTitle("Create case", for: .normal)
        doneButton.backgroundColor = .white
        doneButton.tintColor = .white
        doneButton.setTitleColor(.lightGray, for: .normal)
        doneButton.setTitleColor(.black, for: .selected)
        doneButton.titleLabel?.backgroundColor = .clear
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        doneButton.isHidden = manager?.caseDetail.checkIsCorrect() ?? false
        if !(manager?.caseDetail.checkIsCorrect() ?? false) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func addTapped() {
        manager?.caseDetail = CaseListModel.CaseModel()
        isDoneUpload = false
        caseDetailsImage.image = UIImage(named: "image-placeholder")
        caseDetailsTextView.text = "Please put you case details"
        caseDetailsTextView.textColor = .gray
        doneButton.isHidden = false
        caseDetailsTextView.isUserInteractionEnabled = true
        selectImageButton.isUserInteractionEnabled = true
    }
    
    @objc func logoutTapped() {
        let alert = UIAlertController(title: "", message: "Do you want to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            LoginModel.profileInfo = nil
            let sView = SplashscreenViewController()
            Utils.changeRootViewController(sView)
        }))
        self.present(alert, animated: true)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        self.view.frame.origin.y = 0 - keyboardSize.height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    private func routeToRegister() {
        let view = RegisterViewController()
        view.modalPresentationStyle = .fullScreen
        self.present(view, animated: true, completion: nil)
    }
    
    @IBAction func createACtionHandler(_ sender: Any) {
        if (manager?.caseDetail.message ?? "") != "" && caseDetailsImage.image != nil {
            guard let img = self.selectImage else { return }
            doneButton.isUserInteractionEnabled = false
            doneButton.setTitle("Loading...", for: .normal)
            manager?.addCases(selectImg: img) { [weak self] status in
                
                if status {
                    let alert = UIAlertController(title: "Your case saved success", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self?.present(alert, animated: true)
                } else {
                    let alert = UIAlertController(title: "Plese check your information", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self?.present(alert, animated: true)
                }
                self?.doneButton.isUserInteractionEnabled = true
                self?.doneButton.isHidden = true
                self?.caseDetailsTextView.isUserInteractionEnabled = false
                self?.selectImageButton.isUserInteractionEnabled = false
                self?.isDoneUpload = true
            }
        } else {
            let alert = UIAlertController(title: "Plese check your information", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func choochImageActionHandler(_ sender: Any) {
        if !isDoneUpload {
            let alert = UIAlertController(title: "", message: "Which way do want to use image from?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { action in
                self.routeToCamera(isCamera: true)
            }))
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { action in
                self.routeToCamera(isCamera: false)
            }))
            self.present(alert, animated: true)
        }
    }
    
    func routeToCamera(isCamera: Bool) {
        vc = UIImagePickerController()
        vc?.sourceType = isCamera ? .camera : .photoLibrary
        vc?.allowsEditing = true
        vc?.delegate = self
        guard let imageVc = vc else { return }
        present(imageVc, animated: true)
    }
    
}

extension CaseDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        selectImage = image
        caseDetailsImage.image = selectImage
        print(image.size)
    }
    
}

extension CaseDetailsViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        caseDetailsTextView.textColor = .darkGray
        caseDetailsTextView.text = manager?.caseDetail.message
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        manager?.caseDetail.message = caseDetailsTextView.text
    }
}
