//
//  RegisterViewController.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstnameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastnameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var ageTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var doctorButton: UIButton!
    @IBOutlet weak var patientButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    var manager: RegisterManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = RegisterManager()
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        roue
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    @IBAction func genderActionHandler(_ sender: UIButton) {
        var isMale: Bool?
        if sender.tag == 0 { // 0: male
            isMale = true
        } else { //  1: female
            isMale = false
        }
        manager?.accountModel.Gender = isMale! ? "M" : "F"
        maleButton.isSelected = isMale!
        femaleButton.isSelected = !isMale!
        maleButton.layer.borderColor = isMale! ? UIColor.black.cgColor : UIColor.lightGray.cgColor
        femaleButton.layer.borderColor = isMale! ? UIColor.lightGray.cgColor : UIColor.black.cgColor
        registerButton.isUserInteractionEnabled = manager?.accountModel.checkIsCorrect() ?? false
    }
    
    @IBAction func typeActionHandler(_ sender: UIButton) {
        // 0: doctor, 1: patient
        var isDoctor: Bool?
        if sender.tag == 0 { // 0: doctor
            isDoctor = true
        } else { //  1: patient
            isDoctor = false
        }
        manager?.accountModel.Status = isDoctor! ? "D" : "P"
        doctorButton.isSelected = isDoctor!
        patientButton.isSelected = !isDoctor!
        doctorButton.layer.borderColor = isDoctor! ? UIColor.black.cgColor : UIColor.lightGray.cgColor
        patientButton.layer.borderColor = isDoctor! ? UIColor.lightGray.cgColor : UIColor.black.cgColor
        registerButton.isUserInteractionEnabled = manager?.accountModel.checkIsCorrect() ?? false
    }
    
    private func isLoading(_ loading: Bool) {
        indicatorView.isHidden = !loading
        registerButton.isUserInteractionEnabled = !loading
    }
    
    @IBAction func backActionHandler(_ sender: Any) {
        self.routeToLogin()
    }
    
    @IBAction func RegisterActionHandler(_ sender: Any) {
        isLoading(true)
        manager?.addAccount(success: { state in
            self.isLoading(false)
            if state {
                self.routeToLogin()
            }
        })
    }
    
    private func routeToLogin() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == usernameTextField {
            manager?.accountModel.Usr = textField.text ?? ""
        }
        if textField == passwordTextField {
            manager?.accountModel.PW = textField.text ?? ""
        }
        if textField == firstnameTextField {
            manager?.accountModel.Name = textField.text ?? ""
        }
        if textField == lastnameTextField {
            manager?.accountModel.Surname = textField.text ?? ""
        }
        if textField == ageTextField {
            manager?.accountModel.Age = Int(textField.text ?? "0")
        }
        if textField == emailTextField {
            manager?.accountModel.Email = textField.text ?? ""
        }
        if textField == phoneTextField {
            manager?.accountModel.Phone = textField.text ?? ""
        }
        registerButton.isUserInteractionEnabled = manager?.accountModel.checkIsCorrect() ?? false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        } else {
            return true
        }
    }
}

extension RegisterViewController {
    private func setupView() {
        self.view.backgroundColor = UIColor.hex("#F2F3F7")
        
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(.gray, for: .normal)
        backButton.tintColor = .white
        backButton.titleLabel?.backgroundColor = .clear
        maleButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        
        indicatorView.style = .large
        indicatorView.color = .gray
        indicatorView.isHidden = true
        indicatorView.startAnimating()
        
        titleLabel.text = "Register"
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        titleLabel.textColor = .darkText
        titleLabel.textAlignment = .center
        
        usernameTextField.placeholder = "Username"
        usernameTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        usernameTextField.lineColor = .darkGray
        usernameTextField.textColor = .darkText
        usernameTextField.placeholderColor = .gray
        usernameTextField.textAlignment = .center
        usernameTextField.delegate = self
        
        passwordTextField.placeholder = "Password"
        passwordTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        passwordTextField.lineColor = .darkGray
        passwordTextField.textColor = .darkText
        passwordTextField.placeholderColor = .gray
        passwordTextField.textAlignment = .center
        passwordTextField.isSecureTextEntry = true
        passwordTextField.delegate = self
        
        firstnameTextField.placeholder = "Firstname"
        firstnameTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        firstnameTextField.lineColor = .darkGray
        firstnameTextField.textColor = .darkText
        firstnameTextField.placeholderColor = .gray
        firstnameTextField.textAlignment = .center
        firstnameTextField.delegate = self
        
        lastnameTextField.placeholder = "Lastname"
        lastnameTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        lastnameTextField.lineColor = .darkGray
        lastnameTextField.textColor = .darkText
        lastnameTextField.placeholderColor = .gray
        lastnameTextField.textAlignment = .center
        lastnameTextField.delegate = self
        
        ageTextField.placeholder = "Age"
        ageTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        ageTextField.lineColor = .darkGray
        ageTextField.textColor = .darkText
        ageTextField.placeholderColor = .gray
        ageTextField.textAlignment = .center
        ageTextField.keyboardType = .numberPad
        ageTextField.delegate = self
        
        emailTextField.placeholder = "Email"
        emailTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        emailTextField.lineColor = .darkGray
        emailTextField.textColor = .darkText
        emailTextField.placeholderColor = .gray
        emailTextField.textAlignment = .center
        emailTextField.keyboardType = .emailAddress
        emailTextField.delegate = self
        
        phoneTextField.placeholder = "Phone"
        phoneTextField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        phoneTextField.lineColor = .darkGray
        phoneTextField.textColor = .darkText
        phoneTextField.placeholderColor = .gray
        phoneTextField.textAlignment = .center
        phoneTextField.keyboardType = .phonePad
        phoneTextField.delegate = self
        
        maleButton.layer.cornerRadius = 22
        maleButton.layer.borderColor = UIColor.lightGray.cgColor
        maleButton.layer.borderWidth = 1
        maleButton.setTitle("Male", for: .normal)
        maleButton.setTitleColor(.lightGray, for: .normal)
        maleButton.setTitleColor(.black, for: .selected)
        maleButton.backgroundColor = .white
        maleButton.tintColor = .white
        maleButton.titleLabel?.backgroundColor = .clear
        maleButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        
        femaleButton.layer.cornerRadius = 22
        femaleButton.layer.borderColor = UIColor.lightGray.cgColor
        femaleButton.layer.borderWidth = 1
        femaleButton.setTitle("Female", for: .normal)
        femaleButton.backgroundColor = .white
        femaleButton.tintColor = .white
        femaleButton.setTitleColor(.lightGray, for: .normal)
        femaleButton.setTitleColor(.black, for: .selected)
        femaleButton.titleLabel?.backgroundColor = .clear
        femaleButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        
        doctorButton.layer.cornerRadius = 22
        doctorButton.layer.borderColor = UIColor.lightGray.cgColor
        doctorButton.layer.borderWidth = 1
        doctorButton.setTitle("Doctor", for: .normal)
        doctorButton.backgroundColor = .white
        doctorButton.tintColor = .white
        doctorButton.setTitleColor(.lightGray, for: .normal)
        doctorButton.setTitleColor(.black, for: .selected)
        doctorButton.titleLabel?.backgroundColor = .clear
        doctorButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        
        patientButton.layer.cornerRadius = 22
        patientButton.layer.borderColor = UIColor.lightGray.cgColor
        patientButton.layer.borderWidth = 1
        patientButton.tintColor = .white
        patientButton.backgroundColor = .white
        patientButton.setTitle("Patient", for: .normal)
        patientButton.setTitleColor(.lightGray, for: .normal)
        patientButton.setTitleColor(.black, for: .selected)
        patientButton.titleLabel?.backgroundColor = .clear
        patientButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        
        registerButton.backgroundColor = .white
        registerButton.layer.cornerRadius = 22
        registerButton.layer.borderColor = UIColor.white.cgColor
        registerButton.layer.borderWidth = 1
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(.lightGray, for: .normal)
        registerButton.setTitleColor(.black, for: .selected)
        registerButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        registerButton.layer.applyShadow(color: .gray, alpha: 0.25)
        registerButton.isUserInteractionEnabled = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.view.addGestureRecognizer(tap)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        self.view.frame.origin.y = 0 - keyboardSize.height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
}
