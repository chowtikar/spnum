//
//  RegisterManager.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import FirebaseDatabase

class RegisterManager {
    
    let database = Database.database().reference(withPath: "Account")
    var accountModel = LoginModel.AccountModel()
    
    func getCurrent(success: @escaping ((Int) -> Void)) {
        database.observeSingleEvent(of: .value) { data in
            success(data.children.allObjects.count)
            self.database.cancelDisconnectOperations()
        }
        success(-1)
    }
    
    func addAccount(success: @escaping ((Bool) -> Void)) {
        if self.accountModel.checkIsCorrect() {
            getCurrent { current in
                if current != -1 {
                    let accountNumber = "Acc\(current + 1)"
                    self.accountModel.ID = accountNumber
                    let dataInfo = [
                        "Age": self.accountModel.Age!,
                        "Date": NSDate().description,
                        "Email": self.accountModel.Email!,
                        "Gender": self.accountModel.Gender!,
                        "ID": self.accountModel.ID!,
                        "Name": self.accountModel.Name!,
                        "PW": self.accountModel.PW!,
                        "Phone": self.accountModel.Phone!,
                        "Status": self.accountModel.Status!,
                        "Surname": self.accountModel.Surname!,
                        "Usr": self.accountModel.Usr!
                        ] as [String : Any]
                    self.database.child(accountNumber).setValue(dataInfo)
                    success(true)
                }
            }
        } else {
            success(false)
        }
    }
}


//self.accountModel = LoginModel.AccountModel(Age: 22, Date: "20/10/20", Email: "test\(numberTest)@gmail.com", Gender: "Female", ID: "ACC\(Int(numberTest) ?? 99)", Name: "name\(numberTest)", PW: "1234", Phone: "0811234\(numberTest)", Status: "P", Surname: "Surname\(numberTest)", Time: "23:10", Usr: accountNumber)
