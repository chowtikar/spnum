//
//  UserDetailsViewController.swift
//
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class UserDetailsViewController: BaseViewController {
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var mailTitleLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var genderTitleLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageTitleLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    var manager: UserDetailsManager?
    var userInfo: LoginModel.AccountModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = UserDetailsManager()
        setupView()
    }
    
    func setupUser(info: LoginModel.AccountModel) {
        userInfo = info
    }
    
    private func setupView() {
        title = "Patient Profile"
        self.view.backgroundColor = .white
        
        nameTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        nameTitleLabel.textColor = .gray
        nameTitleLabel.text = "Fullname:"
        
        fullNameLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        fullNameLabel.textColor = .darkGray
        fullNameLabel.text = "\(userInfo?.Name ?? "") \(userInfo?.Surname ?? "")"
        
        mailTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        mailTitleLabel.textColor = .gray
        mailTitleLabel.text = "E-mail:"
        
        mailLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        mailLabel.textColor = .darkGray
        mailLabel.text = userInfo?.Email ?? "-"
        
        phoneTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        phoneTitleLabel.textColor = .gray
        phoneTitleLabel.text = "Phone number:"
        
        phoneLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        phoneLabel.textColor = .darkGray
        phoneLabel.text = userInfo?.Phone ?? "-"
        
        genderTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        genderTitleLabel.textColor = .gray
        genderTitleLabel.text = "Gender:"
        
        genderLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        genderLabel.textColor = .darkGray
        genderLabel.text = userInfo?.Gender == "M" ? "Male" : "Female"
        
        ageTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        ageTitleLabel.textColor = .gray
        ageTitleLabel.text = "Age:"
        
        ageLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        ageLabel.textColor = .darkGray
        ageLabel.text = "\(userInfo?.Age ?? 0) years"
    }
}
