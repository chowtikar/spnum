//
//  UserDetailsManager.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 19/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import FirebaseDatabase
import CodableFirebase
import FirebaseStorage

class UserDetailsManager {

    let storage = Storage.storage().reference()
    
    var caseDetail = CaseListModel.CaseModel()
    
    func getCurrentCase(success: @escaping ((Int) -> Void)) {
        let database = Database.database().reference(withPath: "Case")
        database.observeSingleEvent(of: .value) { data in
            success(data.children.allObjects.count)
            database.cancelDisconnectOperations()
        }
        success(-1)
    }
    
    func uploadMedia(selectedImage: UIImage, completion: @escaping (_ url: String?) -> Void) {
        let imgName = self.getImageName()
        if imgName != "" {
            let fullNameImage = "images/\(imgName).png"
            if let imageData = selectedImage.pngData() {
                storage.child(fullNameImage).putData(imageData, metadata: nil) { (_, error) in
                    guard error == nil else {
                        print ("fail to upload")
                        return
                    }
                    self.storage.child(fullNameImage).downloadURL { (url, err) in
                        guard let url = url, err == nil else { return }
                        completion(url.absoluteString)
                    }
                    
                }
            }
        }
    }
    
    func getImageName() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy.MM.dd-HH:mm:ssZ"
        
        if let date = dateFormatterGet.date(from: NSDate().description) {
            let id = LoginModel.profileInfo?.ID ?? "00"
            return ("\(id)_\(dateFormatterPrint.string(from: date))")
        } else {
            return ""
        }
    }
    
    func addCases(selectImg: UIImage, success: @escaping ((Bool) -> Void)) {
        getCurrentCase { current in
            if current != -1 {
                let caseNumberId = "Case\(current + 1)"
                self.caseDetail.id = caseNumberId
                if self.caseDetail.checkIsCorrect() {
                    self.uploadMedia(selectedImage: selectImg) { imgUrl in
                        self.caseDetail.imageUrl = imgUrl
                        let database = Database.database().reference(withPath: "Case")
                        database.child(caseNumberId).setValue(self.caseDetail.getDict())
                        success(true)
                    }
                } else {
                    success(false)
                }
            } else {
                print("what the fuck")
            }
        }
    }
}
