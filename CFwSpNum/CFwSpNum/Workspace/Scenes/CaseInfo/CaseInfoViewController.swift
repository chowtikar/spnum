//
//  CaseInfoViewController.swift
//
//
//  Created by Chotikar Pho. on 17/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

class CaseInfoViewController: BaseViewController {
    
    @IBOutlet weak var caseDetailsImage: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    
    var caseInformation: CaseListModel.CaseModel?
    var manager: CaseInfoManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard (manager == nil) else { return }
        manager = CaseInfoManager()
        setupView()
        resetData()
    }
    
    func updateInfomation(caseInfo: CaseListModel.CaseModel) {
        caseInformation = caseInfo
    }
    
    func resetData() {
        guard let info = caseInformation else { return }
        guard let imgUrl = info.imageUrl else { return }
        let url = URL(string: imgUrl)!
        downloadImage(from: url)
        fullNameLabel.text = "\(info.name ?? "") \(info.lastname ?? "")"
        descriptionLabel.text = "Age: \(info.age)     Gender: \(info.gender == "M" ? "Male" : "Female")"
        detailsTextView.text = info.message
        doneButton.isHidden = info.isDone
    }
    
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.caseDetailsImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    @IBAction func createACtionHandler(_ sender: UIButton) {
        manager?.updateCaseDone(caseId: caseInformation?.id ?? "", successHandle: {
            self.doneButton.isHidden = true
            let alert = UIAlertController(title: "update success", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }, errorHandle: { message in
            let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        })
    }
    
    private func setupView() {
        self.view.backgroundColor = .white
        title = "Case Details"
        caseDetailsImage.image = UIImage(named: "image-placeholder")
        caseDetailsImage.contentMode = .scaleAspectFit
        
        fullNameLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        fullNameLabel.textColor = .darkText
        fullNameLabel.text = "-"
        
        descriptionLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        descriptionLabel.textColor = .gray
        descriptionLabel.text = "Age: -     Gender: -"
        
        detailsTextView.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        detailsTextView.layer.cornerRadius = 16
        detailsTextView.clipsToBounds = true
        detailsTextView.textColor = .darkText
        detailsTextView.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        detailsTextView.backgroundColor = .viewBackground
        detailsTextView.isUserInteractionEnabled = false
        
        doneButton.layer.cornerRadius = 22
        doneButton.layer.borderColor = UIColor.lightGray.cgColor
        doneButton.layer.borderWidth = 1
        doneButton.setTitle("Done case", for: .normal)
        doneButton.backgroundColor = .white
        doneButton.tintColor = .white
        doneButton.setTitleColor(.black, for: .normal)
        doneButton.titleLabel?.backgroundColor = .clear
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
    }
}
