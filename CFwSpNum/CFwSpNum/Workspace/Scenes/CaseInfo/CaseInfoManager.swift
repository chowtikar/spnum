//
//  CaseInfoManager.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 25/10/2563 BE.
//

import Firebase

class CaseInfoManager {
    
    let database = Database.database()
    
    func updateCaseDone(caseId: String, successHandle: (() -> Void)?, errorHandle: ((String) -> Void)? = nil) {
        database.reference(withPath: "Case").child(caseId).child("isDone").setValue(true) { error, _  in
            if let err = error {
                errorHandle?(err.localizedDescription)
            } else {
                successHandle?()
            }
        }
    }    
}
