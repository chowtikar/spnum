//
//  Utils.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 24/10/2563 BE.
//

import Foundation

struct Utils {
    private init() { }
    
    static func changeRootViewController(_ rootViewController: UIViewController, animated: Bool = true, from: UIViewController? = nil, completion: ((Bool) -> Void)? = nil) {
        let window = UIApplication.shared.keyWindow ?? UIApplication.shared.delegate?.window ?? nil
        if let window = window, animated {
            UIView.transition(with: window, duration: 0.175, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                window.rootViewController = rootViewController
                window.makeKeyAndVisible()
                UIView.setAnimationsEnabled(oldState)
            }, completion: completion)
        } else {
            window?.rootViewController = rootViewController
        }
    }
    
}
