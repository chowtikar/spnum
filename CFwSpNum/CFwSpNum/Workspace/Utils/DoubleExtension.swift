//
//  DoubleExtension.swift
//  CFwSpNum
//
//  Created by Chotikar Pho. on 27/6/2563 BE.
//  Copyright © 2563 True-e-logistics. All rights reserved.
//

import Foundation

extension Double {
    
    var numberFormate: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
        return numberFormatter.string(from: self as NSNumber)!
    }
}
